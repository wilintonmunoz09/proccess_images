const tf = require('@tensorflow/tfjs-node');
const posenet = require('@tensorflow-models/posenet');
const {
  createCanvas, Image
} = require('canvas');
const imageScaleFactor = 0.5;
const outputStride = 16;
const fs = require('fs');
const flipHorizontal = false;
global.atob = require('atob');
global.btoa = require('btoa');

const pose = async (imgsrc, filename, _file) => {
  console.log("Start");
  const img = new Image();
  img.src = imgsrc;
  const canvas = createCanvas(img.width, img.height);
  const ctx = canvas.getContext('2d');
  ctx.drawImage(img, 0, 0);
  const net = await posenet.load({
    architecture: 'ResNet50',
    inputResolution: { width: img.width, height: img.height },
    outputStride: 32
  });
  const input = tf.browser.fromPixels(canvas);
  const pose = await net.estimateSinglePose(input, imageScaleFactor, flipHorizontal, outputStride);
  ctx.beginPath();
  let fileOutput = "";
  for (var keypoint of pose.keypoints) {
    fileOutput += `${keypoint.part}: (${keypoint.position.x},${keypoint.position.y})\n`
  }
  fs.write('output/'+filename+"/"+_file.split('.')[0]+".txt", fileOutput, function(err, data) {});
}

function readFolder() {
  var files = [];
  fs.readdir('in/', function (err, _files) {
    if (err) {
      return console.log('Unable scan directory');
    }

    _files.forEach(function (file) {
      var filename = file.split('.')[0]
      fs.mkdirSync('output/' + filename, function (response) {
        console.log(file + " - " + response);
      });
      files.push(file);
    });
  });
  return files;
}

function processVideo(pathVideo, outputFolder) {
  try {
    var process = new ffmpeg(pathVideo);
    process.then(function (video) {
      video.fnExtractFrameToJPG(outputFolder, {
        every_n_frames: 1
      }, function () { })
    }, function (err) {
      console.log('Error: ' + err);
    });
  } catch (e) {
    console.log(e.code);
    console.log(e.msg);
  }
}

function main() {
  var imagesDir = [];

  const files = readFolder();
  files.forEach(function (file) {
    var filename = file.split('.')[0];
    processVideo("in/" + file, "output/" + filename + "/");
    fs.readdir('output/' + filename + "/", function (err, _files) {
      if (err) {
        return console.log('Unable scan directory');
      }

      _files.forEach(function (_file) {
        imagesDir.push({
          patch: 'output/' + filename + '/' + _file,
          filename: filename,
          _file: _file
        });
      });
    });
  });

  imagesDir.forEach(function (image) {
    pose(image.patch, image.filename, image._file);
  });
}

main();